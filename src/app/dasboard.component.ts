import {Component, OnInit} from '@angular/core'
import {Hero} from './hero'
import {HeroService} from './hero.service'
@Component(
    {
        selector:'dasboard',
        templateUrl: './templates/dashboard.html',
        styleUrls: [ './dashboard.component.css' ]
    }
)

export class DashboardComponent implements OnInit{
    heros:Hero[]=[];
    
    constructor(private heroService: HeroService){}

    ngOnInit():void{
        this.heroService.getHeros().then(hrs=>this.heros=hrs.slice(1,5))
    }
}
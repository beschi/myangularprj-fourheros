import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router'; // To read parameters from URL
import { Location } from '@angular/common'; // To read parameters from URL
import { Hero } from './hero';
import { HeroService } from './hero.service'
import 'rxjs/add/operator/switchMap';//To use with route parameters observable

@Component({
    selector: 'hero-detail',
    templateUrl: './templates/heroDetail.html',
    styleUrls:['./hero-detail.component.css']
})

export class HeroDetailComponent implements OnInit {
    //@Input() hero : Hero;
    hero: Hero;
    constructor(
        private activatedRoute: ActivatedRoute,
        private heroService: HeroService,
        private location: Location
    ) { }

    ngOnInit(): void {
        this.activatedRoute.params
            .switchMap((params: Params) => this.heroService.getHero(+params['id']))
            .subscribe(hero => this.hero = hero);
    }

    goBack(): void {
        this.location.back();
    }

    save():void{
        this.heroService.update(this.hero).then(()=>this.goBack());
    }
}
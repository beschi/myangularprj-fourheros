import {Component} from '@angular/core'

@Component({
    selector:'my-app',
    template:`<h1>{{title}}</h1>
        <!--<my-heros></my-heros>-->
        <nav>
        <a routerLink="/dashboard" routerLinkActive="active">DASHBOARD</a>
        <a routerLink="/heroes" routerLinkActive="active">HEROS</a>
        </nav>
        <router-outlet></router-outlet>
    `,
    styleUrls:['./app.component.css']
})


export class AppComponent{
    title: string;

    constructor(){
        this.title ="Angular JS";
    }
}
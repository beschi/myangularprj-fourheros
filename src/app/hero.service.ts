import { Injectable } from '@angular/core'
import { Headers,Http } from '@angular/http'
import { Hero } from './hero'

import 'rxjs/add/operator/toPromise';

@Injectable()
export class HeroService {
    private herosUrl = 'api/blala';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http:Http){}

    getHeros(): Promise<Hero[]> {
        return this.http.get(this.herosUrl)
               .toPromise()
               .then(response => response.json().data as Hero[])
               .catch(this.handleError);
                   
    };

    getHeroesSlowly(): Promise<Hero[]> {
        return new Promise(resolve => {
            // Simulate server latency with 2 second delay
            setTimeout(() => resolve(this.getHeros()), 5000);
        });
    }

    getHero(id: number): Promise<Hero> {
        return this.getHeros().then(heroes => heroes.find(hero => hero.id === id));
    }

   

    update(hero: Hero): Promise<Hero> {
        const url = `${this.herosUrl}/${hero.id}`;
        return this.http
            .put(url, JSON.stringify(hero), {headers: this.headers})
            .toPromise()
            .then(() => hero)
            .catch(this.handleError);
    }

    create(heroname:string):Promise<Hero>{
        return this.http
            .post(this.herosUrl,JSON.stringify({name:heroname}),{headers: this.headers})
            .toPromise()
            .then(res=>res.json().data as Hero)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        const url = `${this.herosUrl}/${id}`;
        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
}

    private handleError(error:any):Promise<any>{
        console.error('An error occurred', error); // for testing purpose
        return Promise.reject(error.message || error);
    }

}
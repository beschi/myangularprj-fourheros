import {Component, OnInit} from '@angular/core'
import {Router} from '@angular/router'
import {Hero} from './hero'
import {HeroService} from './hero.service'

@Component({
  selector:'my-heros',
  templateUrl :'./templates/heros.html'
})


export class HeroesComponent implements OnInit {
  title: string;
  hero: Hero;
  heros: Hero[];
  
  constructor(
    private heroService: HeroService,
    private router:Router
    ){ }

  onSelect(hero: Hero): void {
      this.hero = hero;
  }

  gotoDetail(){
    this.router.navigate(['/detail', this.hero.id]);
  }

  ngOnInit():void{
    //Resolving the promise, or handling the success callback with "then"
    this.heroService.getHeros().then(hrs=>this.heros=hrs);  
    //this.heroService.getHeroesSlowly().then(hrs=>this.heros=hrs);  
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
  this.heroService.create(name)
    .then(h => {
      this.heros.push(h);
      this.hero = null;
    });
  }

  delete(hr: Hero): void {
  this.heroService
      .delete(hr.id)
      .then(() => {
        this.heros = this.heros.filter(h => h !== hr);
        if (this.hero === hr) { this.hero = null; }
      });
  }
}